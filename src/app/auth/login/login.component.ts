import { Component, OnInit } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class LoginComponent extends NbLoginComponent implements OnInit {

  user: any;

  ngOnInit() {
  }

  login() {
    if ( this.user.username !== '' && this.user.password !== '' ) {
      this.service.authenticate('username', this.user).subscribe(
        data => {
          localStorage.setItem('token', JSON.parse(JSON.stringify(data)).token.token);
          localStorage.setItem('role', JSON.parse(JSON.stringify(data)).response.body.role );
          localStorage.setItem('username', JSON.parse(JSON.stringify(data)).response.body.username );

          this.router.navigate(['/pages/']);
        },
        error => {
          console.log(error);
        }
      )
    }
  }

}
