export interface Brand {
    id?: number;
    name: string;
    status: string;
    products?: any;
}