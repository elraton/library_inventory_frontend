export interface Category {
    id?: number;
    name: string;
    status: string;
    products?: any;
}