export interface Products {
    id: number;
    image: string;
    name: string;
    code: string;
    lot: number;
    price: number;
    unit_price: number;
    quantity: number;
    quantity_actual: number;
    description: string;
    category: any;
    brand: any;
    store: any;
}