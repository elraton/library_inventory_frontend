export interface Store {
    id?: number;
    name: string;
    status: string;
    products?: any;
}