import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Brand } from '../../../models/brand';
import { BrandService } from '../brand.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'ngx-brand-add',
  templateUrl: 'add.component.html',
})
export class AddBrandComponent {

  form: FormGroup;
  data: Brand;

  constructor(
    private service: BrandService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      status: new FormControl('activo', Validators.required)
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data = {
        name: this.form.get('name').value,
        status: this.form.get('status').value
      };
      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/brand/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }

}
