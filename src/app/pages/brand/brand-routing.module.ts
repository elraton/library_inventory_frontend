import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandComponent } from './brand.component';
import { ListBrandComponent } from './list/list.component';
import { AddBrandComponent } from './add/add.component';
import { EditBrandComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: BrandComponent,
  children: [
    {
      path: 'list',
      component: ListBrandComponent,
    },
    {
      path: 'add',
      component: AddBrandComponent,
    },
    {
      path: 'edit',
      component: EditBrandComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class BrandRoutingModule {}

export const routedComponents = [
  BrandComponent,
  ListBrandComponent,
  AddBrandComponent,
  EditBrandComponent
];