import { NgModule } from '@angular/core';
import { BrandRoutingModule, routedComponents } from './brand-routing.module';
import { AddBrandComponent } from './add/add.component';
import { EditBrandComponent } from './edit/edit.component';
import { ListBrandComponent } from './list/list.component';
import { BrandComponent } from './brand.component';
import { BrandService } from './brand.service';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';

@NgModule({
  imports: [
    BrandRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    BrandService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class BrandModule { }
