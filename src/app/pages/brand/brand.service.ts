import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Brand } from '../../models/brand';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  get(): Observable<Brand[]> {
    return this.http.get<Brand[]>(this.baseurl + 'brand');
  }

  create(data: Brand): Observable<Brand> {
    return this.http.post<Brand>(this.baseurl + 'brand', data);
  }

  update(data: Brand): Observable<Brand> {
    return this.http.put<Brand>(this.baseurl + 'brand/' + data.id, data);
  }

  delete(data: Brand) {
    return this.http.delete(this.baseurl + 'brand/' + data.id);
  }
    
}

