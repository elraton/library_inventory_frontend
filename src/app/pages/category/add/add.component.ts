import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Category } from '../../../models/category';
import { CategoryService } from '../category.service';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-category-add',
  templateUrl: 'add.component.html',
})
export class AddCategoryComponent {

  form: FormGroup;
  data: Category;

  constructor(
    private service: CategoryService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      status: new FormControl('activo', Validators.required)
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data = {
        name: this.form.get('name').value,
        status: this.form.get('status').value
      };
      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/category/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
  
}
