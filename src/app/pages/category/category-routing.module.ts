import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { ListCategoryComponent } from './list/list.component';
import { AddCategoryComponent } from './add/add.component';
import { EditCategoryComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: CategoryComponent,
  children: [
    {
      path: 'list',
      component: ListCategoryComponent,
    },
    {
      path: 'add',
      component: AddCategoryComponent,
    },
    {
      path: 'edit',
      component: EditCategoryComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class CategoryRoutingModule {}