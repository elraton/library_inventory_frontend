import { NgModule } from '@angular/core';
import { CategoryRoutingModule } from './category-routing.module';
import { AddCategoryComponent } from './add/add.component';
import { EditCategoryComponent } from './edit/edit.component';
import { ListCategoryComponent } from './list/list.component';
import { CategoryComponent } from './category.component';
import { CategoryService } from './category.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CategoryRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule
  ],
  declarations: [
    AddCategoryComponent,
    EditCategoryComponent,
    ListCategoryComponent,
    CategoryComponent
  ],
  providers: [
    CategoryService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class CategoryModule { }
