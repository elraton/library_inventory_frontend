import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  get(): Observable<Category[]> {
    return this.http.get<Category[]>(this.baseurl + 'category');
  }

  create(data: Category): Observable<Category> {
    return this.http.post<Category>(this.baseurl + 'category', data);
  }

  update(data: Category): Observable<Category> {
    return this.http.put<Category>(this.baseurl + 'category/' + data.id, data);
  }

  delete(data: Category) {
    return this.http.delete(this.baseurl + 'category/' + data.id);
  }
  
}
