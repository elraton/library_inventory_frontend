import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Category } from '../../../models/category';
import { CategoryService } from '../category.service';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-category-edit',
  templateUrl: 'edit.component.html'
})
export class EditCategoryComponent {

  form: FormGroup;
  data: Category = JSON.parse(localStorage.getItem('edit'));

  constructor(
    private service: CategoryService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      name: new FormControl(this.data.name, Validators.required),
      status: new FormControl(this.data.status, Validators.required)
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data.name = this.form.get('name').value;
      this.data.status = this.form.get('status').value;
      this.service.update(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/category/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
  
}
