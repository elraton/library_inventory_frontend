import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Products } from '../../../models/product';
import { IncomeService } from '../income.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Category } from '../../../models/category';
import { Brand } from '../../../models/brand';
import { Store } from '../../../models/store';
import { CategoryService } from '../../category/category.service';
import { BrandService } from '../../brand/brand.service';
import { StoreService } from '../../store/store.service';

@Component({
  selector: 'ngx-income-add',
  templateUrl: 'add.component.html',
  providers: [CategoryService, BrandService, StoreService]
})
export class AddIncomeComponent {

  form: FormGroup;
  data: Products;

  category_list: Category[] = [];
  brand_list: Brand[] = [];
  store_list: Store[] = [];

  showStore = false;
  storeForm: FormGroup;

  showCategory = false;
  categoryForm: FormGroup;

  showBrand = false;
  brandForm: FormGroup;

  constructor(
    private service: IncomeService,
    private categoryService: CategoryService,
    private brandService: BrandService,
    private storeService: StoreService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
    this.getCategories(0);
    this.getBrands(0);
    this.getStores(0);
  }

  createForms() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      code: new FormControl('', Validators.required),
      lot: new FormControl(1, Validators.required),
      price: new FormControl('', Validators.required),
      unit_price: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),      
      
      category: new FormControl('', Validators.required),
      brand: new FormControl('', Validators.required),
      store: new FormControl('', Validators.required),
    });

    this.storeForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });

    this.categoryForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });

    this.brandForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  getCategories(id: number) {
    this.categoryService.get().subscribe(
      data => {
        this.category_list = data;
        if (id > 0) {
          this.form.get('category').setValue(id);
        } else {
          this.form.get('category').setValue(this.category_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  getBrands(id: number) {
    this.brandService.get().subscribe(
      data => {
        this.brand_list = data;
        if ( id > 0 ) {
          this.form.get('brand').setValue(id);
        } else {
          this.form.get('brand').setValue(this.brand_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  getStores(id: number) {
    this.storeService.get().subscribe(
      data => {
        this.store_list = data;
        if (id > 0) {
          this.form.get('store').setValue(id);
        } else {
          this.form.get('store').setValue(this.store_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  clearForm() {
    this.form.get('name').setValue('');
    this.form.get('code').setValue('');
    this.form.get('price').setValue('');
    this.form.get('unit_price').setValue('');
    this.form.get('quantity').setValue('');
    this.form.get('description').setValue('');
    this.form.get('category').setValue(this.category_list[0].id);
    this.form.get('brand').setValue(this.brand_list[0].id);
    this.form.get('store').setValue(this.store_list[0].id);
  }

  toggleStoreform() {
    this.showStore = !this.showStore;
  }

  toggleBrand() {
    this.showBrand = !this.showBrand;
  }

  toggleCategory() {
    this.showCategory = !this.showCategory;
  }

  saveStore() {
    if(this.storeForm.valid) {
      const data: Store = {
        name: this.storeForm.get('name').value,
        status: 'activo'
      }
      this.storeService.create(data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.storeForm.get('name').setValue('');
          this.getStores(data.id);
          this.toggleStoreform();
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        },
      );
    }
  }

  saveCategory() {
    if ( this.categoryForm.valid ) {
      const data: Category = {
        name: this.categoryForm.get('name').value,
        status: 'activo'
      };

      this.categoryService.create(data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.categoryForm.get('name').setValue('');
          this.getCategories(data.id);
          this.toggleCategory();
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    }
  }

  saveBrand() {
    if ( this.brandForm.valid ) {
      const data: Brand = {
        name: this.brandForm.get('name').value,
        status: 'activo'
      }

      this.brandService.create(data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.brandForm.get('name').setValue('');
          this.getBrands(data.id);
          this.toggleBrand();
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        },
      );
    }
  }

  onSubmit(option: number) {
    if ( this.form.valid ) {
      this.data = {
        brand: this.form.get('brand').value,
        category: this.form.get('category').value,
        code: this.form.get('code').value,
        lot: this.form.get('lot').value,
        description: this.form.get('description').value,
        image: '',
        name: this.form.get('name').value,
        price: this.form.get('price').value,
        quantity: this.form.get('quantity').value,
        quantity_actual: this.form.get('quantity').value,
        store: this.form.get('store').value,
        unit_price: this.form.get('unit_price').value,
        id: 0
      }

      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          if ( option == 0) {
            this.router.navigate(['/pages/income/list']);
            return;
          }
          this.clearForm();
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  onlynumbers(e: KeyboardEvent) {
    console.log(e.keyCode);
    if (
      // Allow: Delete, Backspace, Tab, Escape, Enter
      [46, 8, 9, 27, 13, 190, 110].indexOf(e.keyCode) !== -1 || 
      (e.keyCode === 65 && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.keyCode === 67 && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.keyCode === 86 && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.keyCode === 88 && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.keyCode === 65 && e.metaKey === true) || // Cmd+A (Mac)
      (e.keyCode === 67 && e.metaKey === true) || // Cmd+C (Mac)
      (e.keyCode === 86 && e.metaKey === true) || // Cmd+V (Mac)
      (e.keyCode === 88 && e.metaKey === true) || // Cmd+X (Mac)
      (e.keyCode >= 35 && e.keyCode <= 39) // Home, End, Left, Right
    ) {
      return;  // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
  
}
