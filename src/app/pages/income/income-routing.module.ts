import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncomeComponent } from './income.component';
import { ListIncomeComponent } from './list/list.component';
import { AddIncomeComponent } from './add/add.component';
import { EditIncomeComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: IncomeComponent,
  children: [
    {
      path: 'list',
      component: ListIncomeComponent,
    },
    {
      path: 'add',
      component: AddIncomeComponent,
    },
    {
      path: 'edit',
      component: EditIncomeComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class IncomeRoutingModule {}