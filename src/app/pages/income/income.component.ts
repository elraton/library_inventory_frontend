import { Component } from '@angular/core';

@Component({
  selector: 'ngx-income',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class IncomeComponent {
}
