import { NgModule } from '@angular/core';
import { IncomeRoutingModule } from './income-routing.module';
import { AddIncomeComponent } from './add/add.component';
import { EditIncomeComponent } from './edit/edit.component';
import { ListIncomeComponent } from './list/list.component';
import { IncomeComponent } from './income.component';
import { IncomeService } from './income.service';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';

@NgModule({
  imports: [
    IncomeRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule
  ],
  declarations: [
    AddIncomeComponent,
    EditIncomeComponent,
    ListIncomeComponent,
    IncomeComponent
  ],
  providers: [
    IncomeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class IncomeModule { }
