import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Products } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class IncomeService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  get(): Observable<Products[]> {
    return this.http.get<Products[]>(this.baseurl + 'product/');
  }

  create(data: Products): Observable<Products> {
    return this.http.post<Products>(this.baseurl + 'product/', data);
  }

  update(data: Products): Observable<Products> {
    return this.http.put<Products>(this.baseurl + 'product/' + data.id, data);
  }

  delete(data: Products) {
    return this.http.delete(this.baseurl + 'product/' + data.id);
  }
  
}
