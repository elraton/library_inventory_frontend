import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { IncomeService } from '../income.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'ngx-income-list',
  templateUrl: 'list.component.html'
})
export class ListIncomeComponent {

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      code: {
        title: 'Codigo',
        type: 'string',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      lot: {
        title: 'Lote',
        type: 'string',
      },
      price: {
        title: 'Precio',
        type: 'string',
      },
      quantity: {
        title: 'Cantidad comprada',
        type: 'string',
      },
      quantity_actual: {
        title: 'Cantidad actual',
        type: 'string'
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];

  loading = true;

  constructor(
    private service: IncomeService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.service.get().subscribe(
      data => {
        this.data = data;
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.error(error);
      }
    );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que quiere borrar la tienda?')) {
      this.service.delete(event.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento borrado', '');
          this.data = this.data.filter( x => x !== event.data);
          this.source.empty();
          this.source.load(this.data);
        },
        error => {
          this.showToast(NbToastStatus.WARNING, 'No se pudo Eliminar', '');
        }
      )
    } else {
      event.confirm.reject();
    }
  }

  onEdit(event) {
    localStorage.setItem('edit', JSON.stringify(event.data));
    this.router.navigate(['/pages/income/edit']);
  }

  onCreate(event) {
    this.router.navigate(['/pages/income/add']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
  
}
