import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomeService } from '../../income/income.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { OutcomeService } from '../outcome.service';
import { Router } from '@angular/router';
import { Products } from '../../../models/product';

@Component({
  selector: 'ngx-outcome-add',
  templateUrl: 'add.component.html',
  providers: [IncomeService]
})
export class AddOutcomeComponent {

  form: FormGroup;

  products: Products[] = [];
  products_all: Products[] = [];

  keyword = 'name';

  products_list = [];

  notFound = 'No hubo resultados';

  constructor(
    private productService: IncomeService,
    private toastrService: NbToastrService,
    private service: OutcomeService,
    private router: Router
  ) {
    this.generateData();
  }

  generateData() {
    this.productService.get().subscribe(
      data => {
        const prod_aux = [];
        this.products = data;
        this.products_all = JSON.parse(JSON.stringify(data));
        this.products_all = this.products_all.filter(x => x.quantity_actual > 0);
        for ( const xx of this.products ) {
          const arr = this.products.filter(x => x.name == xx.name);
          let sum = 0;
          if ( arr.length > 1 ) {
            for ( const yy of arr ) {
              sum = sum + yy.quantity_actual;
            }
            xx.quantity_actual = sum;

            const pro_x = prod_aux.find(x => x.name == xx.name);
            if ( pro_x == null) {
              prod_aux.push(xx);
            }
          } else {
            prod_aux.push(xx);
          }
        }
        this.products = prod_aux;
      },
      error => {
        console.log(error);
      }
    );
  }

  addProduct() {
    this.products_list.push(
      {
        product: '',
        quantity: ''
      }
    );
  }

  removeProduct(product) {
    this.products_list = this.products_list.filter( x => x !== product );
  } 

  selectEvent(item) {
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e){
    // do something when input is focused
  }

  getlowestlot(arr) {
    let lot = 99999;
    arr.map( x => {
      if ( x.lot < lot && x.quantity_actual > 0 ) {
        lot = x.lot;
      }
    });
    return lot;
  }

  async save() {
    if ( this.products_list.length > 0 ) {
      let error = false;
      this.products_list.map( x => {
        if ( !x.product ) {
          error = true;
        }
        if ( isNaN( Number(x.quantity) ) ) {
          error = true;
        }

        if ( Number(x.quantity) > x.product.quantity_actual ) {
          error = true;
        }
      });
      if ( !error ) {

        // search lowest lot if quantity is higher go to next lot

        const product_data = [];
        for ( const xx of this.products_list ) {

          const products_2_parse = this.products_all.filter( x => x.code == xx.product.code);
          let lot = this.getlowestlot(products_2_parse);
          let quantity = +xx.quantity;

          let iterations = 0;

          while ( quantity > 0 ) {
            const prod = this.products_all.find( x => x.code == xx.product.code && x.lot == lot);
            if ( prod.quantity_actual >= quantity ) {
              product_data.push({
                product: prod.id,
                quantity: quantity
              });

              prod.quantity_actual = +prod.quantity_actual - quantity;
              await this.productService.update(prod).toPromise().then().catch();
              quantity = 0;
            } else {
              product_data.push({
                product: prod.id,
                quantity: prod.quantity_actual
              });
              quantity = quantity - prod.quantity_actual;
              prod.quantity_actual = 0;
              lot = lot + 1;
              await this.productService.update(prod).toPromise().then().catch();
            }

            iterations++;
            if ( iterations == 10 ) {
              return;
            }
          }
        }

        this.service.create({ products: product_data, status: 'ACTIVO'}).subscribe(
          async data => {

            //for ( const xx of this.products_list ) {
            //  const product = this.products.find(x => x.id == xx.product.id);
            //  product.quantity_actual = +product.quantity_actual - +xx.quantity;
            //  await this.productService.update(product).toPromise().then().catch();
            //}

            this.showToast(NbToastStatus.SUCCESS, 'Se guardo la salida de productos', '');
            this.router.navigate(['/pages/outcome/list']);
          },
          error => {
            this.showToast(NbToastStatus.DANGER, 'No se pudo guardar', '');
          }
        );
        
      } else {
        this.showToast(NbToastStatus.DANGER, 'Uno de los productos tiene un error', '');
      }
    } else {
      this.showToast(NbToastStatus.DANGER, 'Debe agregar productos', '');
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    this.toastrService.show(body, title, config);
  }
}
