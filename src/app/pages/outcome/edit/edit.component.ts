import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomeService } from '../../income/income.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { OutcomeService } from '../outcome.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-outcome-edit',
  templateUrl: 'edit.component.html',
  providers: [IncomeService]
})
export class EditOutcomeComponent {

  Outcome = JSON.parse( localStorage.getItem('edit') );

  form: FormGroup;

  products = [];

  keyword = 'name';

  products_list = [];

  notFound = 'No hubo resultados';

  constructor(
    private productService: IncomeService,
    private toastrService: NbToastrService,
    private service: OutcomeService,
    private router: Router
  ) {
    console.log( JSON.parse( localStorage.getItem('edit') ) );
    this.generateData();
  }

  generateData() {
    this.productService.get().subscribe(
      data => {
        this.products = data;
        this.products_list = JSON.parse(JSON.stringify(this.Outcome.products));
        for (const xx of this.products_list) {
          xx.product = this.products.find( x => x.id == xx.product);
        }

      },
      error => {
        console.log(error);
      }
    );
  }

  addProduct() {
    this.products_list.push(
      {
        product: '',
        quantity: ''
      }
    );
  }

  removeProduct(product) {
    this.products_list = this.products_list.filter( x => x !== product );
  } 

  selectEvent(item, product) {
    product.product = item;
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e){
    // do something when input is focused
  }

  async save() {

    if ( this.products_list.length > 0 ) {
      let error = false;
      this.products_list.map( x => {
        if ( !x.product ) {
          error = true;
        }
        if ( isNaN( Number(x.quantity) ) ) {
          error = true;
        }

        if ( Number(x.quantity) > x.product.quantity_actual ) {
          error = true;
        }
      });
      if ( !error ) {

        const product_data = [];
        for ( const xx of this.products_list ) {
          product_data.push({
            product: xx.product.id,
            quantity: +xx.quantity
          });
        }

        await this.service.delete(this.Outcome).toPromise().then().catch();

        for ( const xx of this.Outcome.products ) {
          const product = this.products.find(x => x.id == xx.product);
          product.quantity_actual = Number(product.quantity_actual) + Number(xx.quantity);
          await this.productService.update(product).toPromise().then().catch();
        }

        this.service.create({ products: product_data, status: 'ACTIVO'}).subscribe(
          async data => {

            for ( const xx of this.products_list ) {
              const product = this.products.find(x => x.id == xx.product.id);
              product.quantity_actual = Number(product.quantity_actual) - Number(xx.quantity);
              await this.productService.update(product).toPromise().then().catch();
            }

            this.showToast(NbToastStatus.SUCCESS, 'Se guardo la salida de productos', '');
            this.router.navigate(['/pages/outcome/list']);
          },
          error => {
            this.showToast(NbToastStatus.DANGER, 'No se pudo guardar', '');
          }
        );
        
      } else {
        this.showToast(NbToastStatus.DANGER, 'Uno de los productos tiene un error', '');
      }
    } else {
      this.showToast(NbToastStatus.DANGER, 'Debe agregar productos', '');
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    this.toastrService.show(body, title, config);
  }
}
