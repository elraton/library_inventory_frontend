import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomeService } from '../../income/income.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { OutcomeService } from '../outcome.service';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { Products } from '../../../models/product';

@Component({
  selector: 'ngx-outcome-list',
  templateUrl: 'list.component.html',
  providers: [IncomeService]
})
export class ListOutcomeComponent {

  data = [];
  data_parsed = [];
  products: Products[] = [];

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      status: {
        title: 'Estado',
        type: 'string',
      },
      createdAt: {
        title: 'Fecha',
        type: 'string'
      },
      products: {
        title: 'Num. Productos',
        type: 'string'
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  loading = true;

  constructor(
    private service: OutcomeService,
    private prodService: IncomeService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.prodService.get().subscribe(
      data => {
        this.products = data;
      },
      error => {}
    );
    this.service.get().subscribe(
      data => {
        this.data = JSON.parse(JSON.stringify(data));
        const data_copy = JSON.parse(JSON.stringify(this.data));

        for ( const xx of data_copy ) {
          xx.products = xx.products.length;
          xx.createdAt = xx.createdAt.split('T')[0];
        }

        this.data_parsed = JSON.parse(JSON.stringify(data_copy));


        this.source.load(this.data_parsed);
        this.loading = false;
      },
      error => {
        console.error(error);
      }
    );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que quieres cancelar el pedido?')) {
      if ( event.data.status !== 'CANCELADO') {
        this.service.delete(event.data).subscribe(
          async datas => {
            const data = this.data.find(x => x.id == event.data.id);
            for ( const xx of data.products ) {
              const prod = this.products.find(x => x.id == xx.product);
              prod.quantity_actual = +prod.quantity_actual + +xx.quantity;
              await this.prodService.update(prod).toPromise().then().catch();
            }
            this.showToast(NbToastStatus.SUCCESS, 'Elemento cancelado', '');
            this.data_parsed.find( x => x == event.data ).status = 'CANCELADO';
            this.source.empty();
            this.source.load(this.data_parsed);
          },
          error => {
            this.showToast(NbToastStatus.WARNING, 'No se pudo Eliminar', '');
          }
        )
      } else {
        this.showToast(NbToastStatus.WARNING, 'Ya esta cancelado', '');
      }
    } else {
      event.confirm.reject();
    }
  }

  onEdit(event) {
    if ( event.data.status !== 'CANCELADO') {
      const data = this.data.find( x => x.id == event.data.id );
      localStorage.setItem('edit', JSON.stringify(data));
      this.router.navigate(['/pages/outcome/edit']);
    } else {
      this.showToast(NbToastStatus.WARNING, 'Orden cancelada', '');
    }
    
  }

  onCreate(event) {
    this.router.navigate(['/pages/outcome/add']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
}
