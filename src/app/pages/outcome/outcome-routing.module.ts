import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OutcomeComponent } from './outcome.component';
import { ListOutcomeComponent } from './list/list.component';
import { AddOutcomeComponent } from './add/add.component';
import { EditOutcomeComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: OutcomeComponent,
  children: [
    {
      path: 'list',
      component: ListOutcomeComponent,
    },
    {
      path: 'add',
      component: AddOutcomeComponent,
    },
    {
      path: 'edit',
      component: EditOutcomeComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class OutcomeRoutingModule {}