import { Component } from '@angular/core';

@Component({
  selector: 'ngx-outcome',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class OutcomeComponent {
}
