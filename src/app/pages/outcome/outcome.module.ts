import { NgModule } from '@angular/core';
import { OutcomeRoutingModule } from './outcome-routing.module';
import { ListOutcomeComponent } from './list/list.component';
import { OutcomeComponent } from './outcome.component';
import { OutcomeService } from './outcome.service';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';
import { AddOutcomeComponent } from './add/add.component';
import { EditOutcomeComponent } from './edit/edit.component';

@NgModule({
  imports: [
    OutcomeRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule,
    AutocompleteLibModule
  ],
  declarations: [
    ListOutcomeComponent,
    AddOutcomeComponent,
    EditOutcomeComponent,
    OutcomeComponent
  ],
  providers: [
    OutcomeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ]
})
export class OutcomeModule { }
