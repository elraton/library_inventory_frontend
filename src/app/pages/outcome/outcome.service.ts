import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OutcomeService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  create(data) {
    return this.http.post(this.baseurl + 'outproduct', data);
  }

  get() {
    return this.http.get(this.baseurl + 'outproduct');
  }

  delete(data) {
    return this.http.delete(this.baseurl + 'outproduct/' + data.id);
  }

  
  
}
