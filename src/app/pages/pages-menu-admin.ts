import { NbMenuItem } from '@nebular/theme';

export const MENU_ADMIN: NbMenuItem[] = [
  {
    title: 'Administrador',
    group: true,
  },
  {
    title: 'Usuarios',
    icon: 'nb-person',
    link: '/pages/users',
    pathMatch: 'store',
  },
  {
    title: 'Tiendas',
    icon: 'nb-layout-default',
    link: '/pages/store',
    pathMatch: 'store',
  },
  {
    title: 'Categorias',
    icon: 'nb-paper-plane',
    link: '/pages/category',
    pathMatch: 'category',
  },
  {
    title: 'Marcas',
    icon: 'nb-tables',
    link: '/pages/brand',
    pathMatch: 'brand',
  },
  {
    title: 'Registro de Productos',
    icon: 'nb-e-commerce',
    link: '/pages/income',
    pathMatch: 'income',
  },
  {
    title: 'Añadir Compras',
    icon: 'nb-e-commerce',
    link: '/pages/shop',
    pathMatch: 'shop',
  },
  {
    title: 'Reportes',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Todos los productos',
        link: '/pages/reports/all',
      },
      {
        title: 'Con menor cantidad',
        link: '/pages/reports/lower',
      },
    ]
  },
  {
    title: 'Usuario',
    group: true,
  },
  {
    title: 'Salida de Productos',
    icon: 'nb-layout-default',
    link: '/pages/outcome',
    home: true,
    pathMatch: 'outcome',
  },
];
