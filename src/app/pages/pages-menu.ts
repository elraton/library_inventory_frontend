import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Salida de Productos',
    icon: 'nb-layout-default',
    link: '/pages/outcome',
    home: true,
    pathMatch: 'outcome',
  },
];
