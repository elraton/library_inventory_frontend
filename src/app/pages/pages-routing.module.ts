import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'users',
      loadChildren: './user/user.module#UserModule',
    },
    {
      path: 'store',
      loadChildren: './store/store.module#StoreModule',
    },
    {
      path: 'category',
      loadChildren: './category/category.module#CategoryModule',
    },
    {
      path: 'brand',
      loadChildren: './brand/brand.module#BrandModule',
    },
    {
      path: 'income',
      loadChildren: './income/income.module#IncomeModule',
    },
    {
      path: 'shop',
      loadChildren: './shop/shop.module#ShopModule',
    },
    {
      path: 'outcome',
      loadChildren: './outcome/outcome.module#OutcomeModule',
    },
    {
      path: 'reports',
      loadChildren: './reports/reports.module#ReportsModule',
    },
    {
      path: '',
      redirectTo: 'outcome',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
