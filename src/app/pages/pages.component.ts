import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { MENU_ADMIN } from './pages-menu-admin';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="role == 'ADMIN' ? menu_admin : menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {
  menu = MENU_ITEMS;
  menu_admin = MENU_ADMIN;
  role = localStorage.getItem('role');
}
