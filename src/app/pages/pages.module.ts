import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './token.interceptor';
import { BrandModule } from './brand/brand.module';
import { StoreModule } from './store/store.module';
import { CategoryModule } from './category/category.module';
import { IncomeModule } from './income/income.module';
import { OutcomeModule } from './outcome/outcome.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    BrandModule,
    StoreModule,
    CategoryModule,
    IncomeModule,
    OutcomeModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class PagesModule {
}
