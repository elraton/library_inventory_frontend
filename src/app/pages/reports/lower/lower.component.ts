import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReportsService } from '../reports.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'ngx-reports-lower',
  templateUrl: 'lower.component.html',
  providers: [ExcelService]
})
export class LowerReportComponent {

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      code: {
        title: 'Codigo',
        type: 'string',
      },
      price: {
        title: 'Precio',
        type: 'string',
      },
      quantity: {
        title: 'Cantidad comprada',
        type: 'string',
      },
      quantity_actual: {
        title: 'Cantidad actual',
        type: 'string'
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];
  data_parsed = [];

  loading = true;

  constructor(
    private service: ReportsService,
    private router: Router,
    private toastrService: NbToastrService,
    private excelService: ExcelService
  ) {
    this.service.lower().subscribe(
      data => {
        this.data = data;
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.error(error);
      }
    );
  }

  export() {
    this.loading = true;
    if ( this.data_parsed.length == 0) {
      for (const xx of this.data) {
        this.data_parsed.push({
          "id": xx.id,
          "Codigo": xx.code,
          "Tienda": xx.store.name,
          "Categoria": xx.category.name,
          "Marca": xx.brand.name,
          "Nombre": xx.name,
          "Descripcion": xx.description,
          "Precio": xx.price,
          "Precio Unitario": xx.unit_price,
          "Cantidad inicial": xx.quantity,
          "Cantidad Actual": xx.quantity_actual
        });
      }
    }
    this.excelService.exportAsExcelFile(this.data_parsed, 'Todos los productos');
    this.loading = false;
  }

  onDeleteConfirm(event): void {
    
  }

  onEdit(event) {
    
  }

  onCreate(event) {
    
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
  
}
