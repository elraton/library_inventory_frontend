import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { AllReportComponent } from './all/all.component';
import { LowerReportComponent } from './lower/lower.component';

const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  children: [
    {
      path: 'all',
      component: AllReportComponent,
    },
    {
      path: 'lower',
      component: LowerReportComponent,
    },
    { path: '', redirectTo: 'all', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ReportsRoutingModule {}