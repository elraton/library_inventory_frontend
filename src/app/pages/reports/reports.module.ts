import { NgModule } from '@angular/core';
import { ReportsRoutingModule } from './reports-routing.module';
import { AllReportComponent } from './all/all.component';
import { LowerReportComponent } from './lower/lower.component';
import { ReportsComponent } from './reports.component';
import { ReportsService } from './reports.service';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';

@NgModule({
  imports: [
    ReportsRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule
  ],
  declarations: [
    AllReportComponent,
    LowerReportComponent,
    ReportsComponent
  ],
  providers: [
    ReportsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class ReportsModule { }
