import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Products } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  all(): Observable<Products[]> {
    return this.http.get<Products[]>(this.baseurl + 'report/all/');
  }

  lower(): Observable<Products[]> {
    return this.http.get<Products[]>(this.baseurl + 'report/lower/');
  }
  
}
