import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop.component';
import { ShopIncomeComponent } from './shop/shop.component';

const routes: Routes = [{
  path: '',
  component: ShopComponent,
  children: [
    {
      path: 'add',
      component: ShopIncomeComponent,
    },
    { path: '', redirectTo: 'add', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ShopRoutingModule {}