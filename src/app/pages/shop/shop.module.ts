import { NgModule } from '@angular/core';
import { ShopRoutingModule } from './shop-routing.module';
import { ShopIncomeComponent } from './shop/shop.component';
import { ShopComponent } from './shop.component';
import { ShopService } from './shop.service';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  imports: [
    ShopRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule,
    AutocompleteLibModule,
  ],
  declarations: [
    ShopIncomeComponent,
    ShopComponent
  ],
  providers: [
    ShopService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class ShopModule { }
