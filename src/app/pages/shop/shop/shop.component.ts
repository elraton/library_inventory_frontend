import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Products } from '../../../models/product';
import { ShopService } from '../shop.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Category } from '../../../models/category';
import { Brand } from '../../../models/brand';
import { Store } from '../../../models/store';
import { CategoryService } from '../../category/category.service';
import { BrandService } from '../../brand/brand.service';
import { StoreService } from '../../store/store.service';
import { IncomeService } from '../../income/income.service';

@Component({
  selector: 'ngx-income-shop',
  templateUrl: 'shop.component.html',
  providers: [CategoryService, BrandService, StoreService, IncomeService]
})
export class ShopIncomeComponent {

  form: FormGroup;

  products: Products[] = [];
  products_all: Products[] = [];

  keyword = 'name';

  product: Products;

  notFound = 'No hubo resultados';

  data: Products;

  category_list: Category[] = [];
  brand_list: Brand[] = [];
  store_list: Store[] = [];

  showStore = false;
  storeForm: FormGroup;

  showCategory = false;
  categoryForm: FormGroup;

  showBrand = false;
  brandForm: FormGroup;

  showForm = false;

  constructor(
    private productService: IncomeService,
    private toastrService: NbToastrService,
    private service: ShopService,
    private categoryService: CategoryService,
    private brandService: BrandService,
    private storeService: StoreService,
    private router: Router
  ) {
    this.generateData();
    this.getCategories(0);
    this.getBrands(0);
    this.getStores(0);
  }

  generateData() {
    this.productService.get().subscribe(
      data => {
        const prod_aux = [];
        this.products = data;
        this.products_all = data;
        for ( const xx of this.products ) {
          const arr = this.products.filter(x => x.name == xx.name);
          let sum = 0;
          if ( arr.length > 1 ) {
            for ( const yy of arr ) {
              sum = sum + yy.quantity_actual;
            }
            xx.quantity_actual = sum;

            const pro_x = prod_aux.find(x => x.name == xx.name);
            if ( pro_x == null) {
              prod_aux.push(xx);
            }
          } else {
            prod_aux.push(xx);
          }
        }
        this.products = prod_aux;
      },
      error => {
        console.log(error);
      }
    );

    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      code: new FormControl('', Validators.required),
      lot: new FormControl(1, Validators.required),
      price: new FormControl('', Validators.required),
      unit_price: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),      
      
      category: new FormControl('', Validators.required),
      brand: new FormControl('', Validators.required),
      store: new FormControl('', Validators.required),
    });

    this.storeForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });

    this.categoryForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });

    this.brandForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  getCategories(id: number) {
    this.categoryService.get().subscribe(
      data => {
        this.category_list = data;
        if (id > 0) {
          this.form.get('category').setValue(id);
        } else {
          this.form.get('category').setValue(this.category_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  getBrands(id: number) {
    this.brandService.get().subscribe(
      data => {
        this.brand_list = data;
        if ( id > 0 ) {
          this.form.get('brand').setValue(id);
        } else {
          this.form.get('brand').setValue(this.brand_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  getStores(id: number) {
    this.storeService.get().subscribe(
      data => {
        this.store_list = data;
        if (id > 0) {
          this.form.get('store').setValue(id);
        } else {
          this.form.get('store').setValue(this.store_list[0].id);
        }
      },
      error => {
        console.error(error);
      },
    );
  }

  getLot(prod) {
    const prod_aux = this.products_all.filter(x => x.name == prod.name);
    let lot_max = 0;
    for ( const xx of prod_aux ) {
      if ( xx.lot > lot_max ) {
        lot_max = xx.lot;
      }
    }
    return lot_max + 1;
  }

  selectEvent(item) {
    this.product = item;
    this.showForm = true;
    
    this.form.get('name').setValue(this.product.name);
    this.form.get('code').setValue(this.product.code);
    this.form.get('lot').setValue(this.getLot(this.product));
    this.form.get('description').setValue(this.product.description);
    this.form.get('category').setValue(this.product.category.id);
    this.form.get('brand').setValue(this.product.brand.id);
    this.form.get('store').setValue(this.product.store.id);
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e){
    // do something when input is focused
  }

  save() {

    if ( this.form.valid ) {
      this.data = {
        brand: this.form.get('brand').value,
        category: this.form.get('category').value,
        code: this.form.get('code').value,
        lot: this.form.get('lot').value,
        description: this.form.get('description').value,
        image: '',
        name: this.form.get('name').value,
        price: this.form.get('price').value,
        quantity: this.form.get('quantity').value,
        quantity_actual: this.form.get('quantity').value,
        store: this.form.get('store').value,
        unit_price: this.form.get('unit_price').value,
        id: 0
      }

      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    this.toastrService.show(body, title, config);
  }
  
}
