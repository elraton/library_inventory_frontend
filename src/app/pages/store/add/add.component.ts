import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StoreService } from '../store.service';
import { Store } from '../../../models/store';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-store-add',
  templateUrl: 'add.component.html',
})
export class AddStoreComponent {

  form: FormGroup;
  data: Store;

  constructor(
    private service: StoreService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      status: new FormControl('activo', Validators.required),
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data = {
        name: this.form.get('name').value,
        status: this.form.get('status').value,
      };
      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/store/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        },
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
}
