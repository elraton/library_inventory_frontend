import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { StoreService } from '../store.service';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-store-list',
  templateUrl: 'list.component.html',
})

export class ListStoreComponent {

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      status: {
        title: 'Estado',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];

  loading = true;

  constructor(
    private service: StoreService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {
    this.service.get().subscribe(
      data => {
        this.data = data;
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.error(error);
      },
    );
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que quiere borrar la tienda?')) {
      this.service.delete(event.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento borrado', '');
          this.data = this.data.filter( x => x !== event.data);
          this.source.empty();
          this.source.load(this.data);
        },
        error => {
          this.showToast(NbToastStatus.WARNING, 'No se pudo Eliminar', '');
        },
      );
    } else {
      event.confirm.reject();
    }
  }

  onEdit(event) {
    localStorage.setItem('edit', JSON.stringify(event.data));
    this.router.navigate(['/pages/store/edit']);
  }

  onCreate(event) {
    this.router.navigate(['/pages/store/add']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }
}
