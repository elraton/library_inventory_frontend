import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreComponent } from './store.component';
import { ListStoreComponent } from './list/list.component';
import { AddStoreComponent } from './add/add.component';
import { EditStoreComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: StoreComponent,
  children: [
    {
      path: 'list',
      component: ListStoreComponent,
    },
    {
      path: 'add',
      component: AddStoreComponent,
    },
    {
      path: 'edit',
      component: EditStoreComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class StoreRoutingModule {}
