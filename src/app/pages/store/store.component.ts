import { Component } from '@angular/core';

@Component({
  selector: 'ngx-store',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class StoreComponent {
}
