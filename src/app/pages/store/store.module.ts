import { NgModule } from '@angular/core';
import { StoreRoutingModule } from './store-routing.module';
import { AddStoreComponent } from './add/add.component';
import { EditStoreComponent } from './edit/edit.component';
import { ListStoreComponent } from './list/list.component';
import { StoreComponent } from './store.component';
import { StoreService } from './store.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    StoreRoutingModule,
    Ng2SmartTableModule,
    ThemeModule,
  ],
  declarations: [
    AddStoreComponent,
    EditStoreComponent,
    ListStoreComponent,
    StoreComponent,
  ],
  providers: [
    StoreService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
})
export class StoreModule { }
