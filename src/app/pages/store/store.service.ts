import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '../../models/store';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoreService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  get(): Observable<Store[]> {
    return this.http.get<Store[]>(this.baseurl + 'store');
  }

  create(data: Store): Observable<Store> {
    return this.http.post<Store>(this.baseurl + 'store', data);
  }

  update(data: Store): Observable<Store> {
    return this.http.put<Store>(this.baseurl + 'store/' + data.id, data);
  }

  delete(data: Store) {
    return this.http.delete(this.baseurl + 'store/' + data.id);
  }
}
