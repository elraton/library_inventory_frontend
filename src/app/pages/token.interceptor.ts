import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import 'rxjs/add/operator/catch';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private router: Router
    ) {}
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: localStorage.getItem('token')
            }
        });

        // return next.handle(request);

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if ( error.status === 401) {
                    this.router.navigate(['/auth/v1']);
                    return throwError(error);
                }
            })
        );


    }
}