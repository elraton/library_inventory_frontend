import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { User } from '../../../models/user';

@Component({
  selector: 'ngx-user-add',
  templateUrl: 'add.component.html',
})
export class AddUserComponent {

  form: FormGroup;
  data: User;

  constructor(
    private service: UserService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      role: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data = {
        username: this.form.get('username').value,
        password: this.form.get('password').value,
        role: this.form.get('role').value
      };
      this.service.create(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/users/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }

}
