import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { Brand } from '../../../models/brand';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { User } from '../../../models/user';

@Component({
  selector: 'ngx-user-edit',
  templateUrl: 'edit.component.html'
})
export class EditUserComponent {

  form: FormGroup;
  data: User = JSON.parse(localStorage.getItem('edit'));

  constructor(
    private service: UserService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.createForms();
  }

  createForms() {
    this.form = new FormGroup({
      username: new FormControl(this.data.username, Validators.required),
      password: new FormControl(''),
      role: new FormControl(this.data.role, Validators.required)
    });
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.data.username = this.form.get('username').value;
      if ( this.form.get('password').value.length > 0 ) {
        this.data.password = this.form.get('password').value;
      }
      this.data.role = this.form.get('role').value;

      this.service.update(this.data).subscribe(
        data => {
          this.showToast(NbToastStatus.SUCCESS, 'Elemento guardado', '');
          this.router.navigate(['/pages/users/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
          console.error(error);
        }
      );
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2500,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    this.toastrService.show(body, title, config);
  }

}
