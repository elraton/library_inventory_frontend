import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { ListUserComponent } from './list/list.component';
import { AddUserComponent } from './add/add.component';
import { EditUserComponent } from './edit/edit.component';

const routes: Routes = [{
  path: '',
  component: UserComponent,
  children: [
    {
      path: 'list',
      component: ListUserComponent,
    },
    {
      path: 'add',
      component: AddUserComponent,
    },
    {
      path: 'edit',
      component: EditUserComponent,
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class UserRoutingModule {}

export const routedComponents = [
  UserComponent,
  ListUserComponent,
  AddUserComponent,
  EditUserComponent
];