import { NgModule } from '@angular/core';
import { UserRoutingModule, routedComponents } from './user-routing.module';
import { AddUserComponent } from './add/add.component';
import { EditUserComponent } from './edit/edit.component';
import { ListUserComponent } from './list/list.component';
import { UserComponent } from './user.component';
import { UserService } from './user.service';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../token.interceptor';

@NgModule({
  imports: [
    UserRoutingModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class UserModule { }
