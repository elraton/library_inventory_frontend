import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseurl;
  constructor(
    private http: HttpClient,
  ) {
    this.baseurl = localStorage.getItem('baseurl');
  }

  get(): Observable<User[]> {
    return this.http.get<User[]>(this.baseurl + 'user');
  }

  getUser(): Observable<User> {
    return this.http.post<User>(this.baseurl + 'user/token', {});
  }

  create(data: User): Observable<User> {
    return this.http.post<User>(this.baseurl + 'user', data);
  }

  update(data: User): Observable<User> {
    return this.http.put<User>(this.baseurl + 'user/' + data.id, data);
  }

  delete(data: User) {
    return this.http.delete(this.baseurl + 'user/' + data.id);
  }
    
}

